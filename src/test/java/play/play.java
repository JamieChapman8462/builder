package play;

import org.junit.Assert;
import org.junit.Test;

public class play {

    private static final String JAMIE = "Jamie";
    private static final String CHAPMAN = "Chapman";

    @Test
    public void testUserBuilder(){
        User.UserBuilder userBuilder = new User.UserBuilder();
        User user = userBuilder.setFirstName(JAMIE).setSurname(CHAPMAN).build();
        Assert.assertEquals(user.getFirstName(), JAMIE);
        Assert.assertEquals(user.getSurname(), CHAPMAN);
    }
}
