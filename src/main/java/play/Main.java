package play;

public class Main {
    public static void main(String[] args) {
        User.UserBuilder userBuilder = new User.UserBuilder();
        User user = userBuilder.setFirstName("Jamie").setSurname("Chapman").build();
        System.out.println(user.getFirstName() + " " + user.getSurname());
    }
}
