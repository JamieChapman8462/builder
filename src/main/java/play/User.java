package play;

class User {
    private final String firstName;
    private final String surname;

    private User(UserBuilder userBuilder) {
        this.firstName = userBuilder.getFirstName();
        this.surname = userBuilder.getSurname();
    }

    String getFirstName() {
        return firstName;
    }

    
    String getSurname() {
        return surname;
    }

    static class UserBuilder {

       private String firstName;
       private String surname;

       String getFirstName() {
           return firstName;
       }

       String getSurname() {
           return surname;
       }

       UserBuilder setFirstName(String firstName) {
            this.firstName = firstName;
            return this;
       }

       UserBuilder setSurname(String surname) {
           this.surname = surname;
           return this;
       }

       User build() {
           return new User(this);
       }
   }
}
